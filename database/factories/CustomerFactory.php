<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'phone_number' => $this->faker->numerify('##########'),
            'first_name' => $this->faker->name(),
            'last_name' => $this->faker->lastname(),
            'email_id' => $this->faker->unique()->safeEmail(),
            'gender' => $this->faker->randomElement(["male", "female", "others"]),
            'date_of_birth' => $this->faker->date($format = 'Y-m-d', $max = '2020',$min = '1960'),
            'is_paid' => $this->faker->randomElement([1,0]),
            'paid_date' =>  $this->faker->date($format = 'Y-m-d H:i:s', $max = '2021',$min = '2010'),
            'receipt' => $this->faker->text(),
            'agent_code' => Str::random(10),
        ];
    }
}
