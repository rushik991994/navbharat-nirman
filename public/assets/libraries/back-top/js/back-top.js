"use strict";

/* Back to Top Scroll S */
    var HeaderHeight = $(".header-mn").height();
    $(window).scroll(function() {
        if ($(this).scrollTop() > HeaderHeight) {
            $('#back_top').show();
        } else {
            $('#back_top').hide();
        }
    });
    $('#back_top').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });
/* Back to Top Scroll E */