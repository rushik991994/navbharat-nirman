/* Home Banner S */
    var owlContactSlideNumber = 1, owlContactClassName = '.bnr-mn';
    var owlContactLoop = false,
        owlContactItemLength = $(owlContactClassName + ' .owl-carousel .item').length,
        owlContactBanner = $(owlContactClassName + ' .owl-carousel');

    if (owlContactItemLength < owlContactSlideNumber) { $(owlContactClassName).find('.owl-controls').css('display', 'none');}
    if (owlContactItemLength > owlContactSlideNumber) { owlContactLoop = true; } else { owlContactLoop = false; }

    owlContactBanner.owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        loop: owlContactLoop,
        mouseDrag: false,
        touchDrag: true,
        margin: 10,
        navText: ['<svg viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
        // autoplay: true,
        autoplayHoverPause: false,
        autoplayTimeout: 5000,
        smartSpeed: 5000,           
        dots: true,
        nav: true,
        responsive: {
            0: { items: 1 },
            480: { items: 1 },
            768: { items: 1 },
            1024: { items: 1 },
            1025: { items: 1, }
        }
    });
/* Home Banner E */