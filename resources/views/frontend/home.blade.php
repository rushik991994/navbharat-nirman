@extends('frontend.layouts.layout')
@section('content')

<!-- Banner S -->
<div class="bnr-mn">
	<div class="owl-carousel owl-theme">
		<div class="item">
			<div class="bnr-sc" style="background-image: url(assets/images/banner-1.jpg);">
				<div class="container">
					<div class="bnr-cnt-sc">
						<h2 class="bnr-ttl">Lorem Ipsum</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
						</p>
						<div class="btn-sc">
							<a href="{{route('apply_portal')}}" class="btn btn-primary">Apply For Portal</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="bnr-sc" style="background-image: url(assets/images/banner-2.jpg);">
				<div class="container">
					<div class="bnr-cnt-sc">
						<h2 class="bnr-ttl">Lorem Ipsum</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
						</p>
						<div class="btn-sc">
							<a href="{{route('apply_portal')}}" class="btn btn-primary">Apply For Portal</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Banner E -->

<!-- Services S -->
<div class="srvc-mn section-gap-padding">
	<div class="container">
		<div class="ttl-sc">
			<h2 class="main-title">Our Services</h2>
		</div>
		<div class="srvc-mn-sc">
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<div class="srvc-sc">
						<span class="srvc-num">01</span>
						<h3 class="srvc-ttl">Unique</h3>
						<p>In terms of planning, design, quality, and sprawling luxurious amenities.</p>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="srvc-sc">
						<span class="srvc-num">02</span>
						<h3 class="srvc-ttl">Quality</h3>
						<p>A quality living from the team that cares.</p>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="srvc-sc">
						<span class="srvc-num">03</span>
						<h3 class="srvc-ttl">Commitment</h3>
						<p>Committed to timely delivery of the Project.</p>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="srvc-sc">
						<span class="srvc-num">04</span>
						<h3 class="srvc-ttl">Key</h3>
						<p>Key to the home of your dreams.</p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="btn-sc text-center">
						<a href="{{route('apply_portal')}}" class="btn btn-primary">Apply for portal</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Services E -->

<!-- Bhk Layouts S -->
<div class="bhk-layout-mn bg-grey section-gap-padding">
	<div class="container">
		<div class="ttl-sc">
			<h2 class="main-title">House Layouts</h2>
		</div>
		<div class="row">
			<div class="col-md-4">
				<a href="javascript:void(0)" class="bhk-sc">
					<div class="img-sc">
						<img src="assets/images/1bhk.jpg" alt="1 Bhk">
					</div>
					<span class="bhk-ttl">1 Bhk Layout</span>
				</a>
			</div>
			<div class="col-md-4">
				<a href="javascript:void(0)" class="bhk-sc">
					<div class="img-sc">
						<img src="assets/images/2bhk.jpg" alt="2 Bhk">
					</div>
					<span class="bhk-ttl">2 Bhk Layout</span>
				</a>
			</div>
			<div class="col-md-4">
				<a href="javascript:void(0)" class="bhk-sc">
					<div class="img-sc">
						<img src="assets/images/3bhk.jpg" alt="3 Bhk">
					</div>
					<span class="bhk-ttl">3 Bhk Layout</span>
				</a>
			</div>
		</div>
		<div class="btn-sc text-center">
			<a href="javascript:void(0)" class="btn btn-primary">Products</a>
		</div>
	</div>
</div>
<!-- Bhk Layouts E -->

<!-- Project gallery S -->
<div class="prjct-gllry-mn">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-5">
				<h2 class="mn-ttl">Project Gallery</h2>
			</div>
			<div class="col-xl-8 col-lg-7">
				<span class="gllry-ttl">WHERE COMFORT AND CONVENIENCE CONVERGE</span>
				<span class="gllry-cnt">Exquisite & Premium 1 BHK / 2 BHK / 3 BHK Residencies Jodhpur</span>
			</div>
		</div>
	</div>
</div>
<!-- Project gallery E -->

<!-- Actual Location S -->
<div class="act-location-mn section-gap-padding bg-grey">
	<div class="container">
		<div class="ttl-sc">
			<h2 class="main-title">Actual Site Photo</h2>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="act-lt-sc">
					<div class="img-sc">
						<img src="assets/images/actual-photo-1.jpg" alt="Actual Photo">
					</div>
					<span class="act-lt-ttl">Actual Photo</span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="act-lt-sc">
					<div class="img-sc">
						<img src="assets/images/actual-photo-2.jpg" alt="Actual Photo">
					</div>
					<span class="act-lt-ttl">Actual Photo</span>
				</div>
			</div>
		</div>
		<div class="btn-sc text-center">
			<a href="javascript:void(0)" class="btn btn-primary">Media Gallery</a>
		</div>
	</div>
</div>
<!-- Actual Location E -->

<!-- Location S -->
<div class="location-mn section-gap-padding">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-md-5">
				<div class="location-sc">
					<div class="ttl-sc">
						<h2 class="main-title">Location Map</h2>
					</div>
					<p>
						Spaces planed to detail, offer and elegant setting for your choice of decor. Warmth from the windows light up the expansive living area. The rooms are designed minimalistic, to provide a soothing ambience to relax.
					</p>
					<div class="btn-sc">
						<a href="/about-us" class="btn btn-primary">Enquiry Now</a>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-md-7">
				<div class="map-sc">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14688.548000996534!2d72.5136932299714!3d23.018741458516068!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e84d196e417db%3A0x60131a3741787591!2sJodhpur%20Village%2C%20Ahmedabad%2C%20Gujarat%20380015!5e0!3m2!1sen!2sin!4v1626320641619!5m2!1sen!2sin" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Location E -->
@endsection