@extends('frontend.layouts.layout')
@section('content')

<!-- Inner Banner S -->
<div class="inr-bnr-mn" style="background-image: url(assets/images/banner-2.jpg);">
	<div class="container">
		<div class="inr-bnr-sc">
			<h1 class="main-title">About Us</h1>
		</div>
	</div>
</div>
<!-- Inner Banner E -->

<!-- About us content S -->
<div class="crm-mn section-gap-padding">
	<div class="container">
		<div class="crm-sc">
			<h2>About Us</h2>
			<p>Bharatavas Yojna Ltd is actively working to provide the housing facility to the people of India with a focus on economically weaker section, lower income groups and middle-income groups of the society with an affordable price, having all the basic amenities required to live a basic lifestyle</p>
			<ul>
				<li>Under this Program a market-place is created by the Bharatavas Yojna Limited which will work to provide the housing units to the EWS/LIG/MIG category families and few other families of the society at a affordable price under various promotional programs run by it. It will be ensured that we should be able to provide houses with all the basic amenities which should lead to better lifestyle of the people. </li>
				<li>Bharatavas Yojna housing program will bridge this gap between the developers and individual/s who want to purchase house, which are already built or are under construction. This housing program will help the developers to sell their current projects to the customers fast which will give them much needed financial support and will further motivate the developers to re-engage in construction of affordable housing and other projects. </li>
				<li>BAYL will ensure and comply with all laws applicable in this process including legal and taxation laws as defined by government of India. </li>
				<li>BAYL will ensure and comply with all laws applicable in this process including legal and taxation laws as defined by government of India. </li>
			</ul>
			<br/><br/>
			<h2>Vision, Mission, and Objectives</h2>
			<h3>Vision</h3>
			<p>Our vision is to provide the housing facility to the people of India with a focus on economically weaker section, lower income groups and middle-income groups of the society with an affordable price, having all the basic amenities required to live a basic lifestyle.</p>
			<br/>
			<h3>Mission</h3>
			<p>Our mission is to develop and introduce housing programme for the people of India wherein, an individual can participate and become eligible to purchase house equipped with the basic amenities required for a family for a better lifestyle at a very reasonable value.</p>
			<br/>
			<h3>Objectives</h3>
			<ul>
				<li>Bringing the available housing projects closer and within the reach of the people of India.</li>
				<li>Providing a platform to the builders and developers for creating EWS/LIG/MIG housing inventory to fulfill housing shortage in the country.</li>
				<li>To provide support to private developers and attract investment for construction of houses for EWS/LIG/MIG segment of the society.</li>
				<li>To achieve the goal of “Affordable Housing for All Indians”</li>
				<li>To support rented individuals to purchase affordable house.</li>
			</ul>
			<div class="btn-sc">
				<a href="{{route('apply_portal')}}" class="btn btn-primary">Apply for portal</a>
			</div>
		</div>
	</div>
</div>
<!-- About us content E -->
@endsection