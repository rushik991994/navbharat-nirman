@extends('frontend.layouts.layout')
@section('content')
<!-- Inner Banner S -->
<div class="inr-bnr-mn" style="background-image: url(assets/images/banner-2.jpg);">
	<div class="container">
		<div class="inr-bnr-sc">
			<h1 class="main-title">Contact Us</h1>
		</div>
	</div>
</div>
<!-- Inner Banner E -->

<!-- Contact Us S -->
<div class="cnt-round-sc section-gap-half-padding">
	<div class="container">
		<ul class="cnt-round-list row">
			<li class="cnt-round-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="cnt-round-box cnt-add d-flex flex-column justify-content-center">
					<span class="cnt-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
					<h2 class="title">Address</h2>
					<p class="content">40 Park Ave, Brooklyn, New York</p>
				</div>
			</li>
			<li class="cnt-round-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="cnt-round-box cnt-phone d-flex flex-column justify-content-center">
					<span class="cnt-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
					<h2 class="title">Call Us Today</h2>
					<a href="javascript:void(0):;" class="content">1-800-111-2222</a>
				</div>
			</li>
			<li class="cnt-round-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="cnt-round-box cnt-email d-flex flex-column justify-content-center">
					<span class="cnt-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
					<h2 class="title">Send Message</h2>
					<a href="javascript:void(0):;" class="content">contact@example.com</a>
				</div>
			</li>
		</ul>
	</div>
</div>
<div class="cnt-form-mn section-gap-padding">
	<div class="container">
		<div class="cnt-form-sc">
			<div class="ttl-sc">
				<h2 class="title main-title">Get in touch with us</h2>
			</div>
			<form class="get-in-form">
				<div class="row">
					<div class="col-lg-6 col-md-12">
						<div class="form-group ad-form-group">
							<label class="nbfrm_label" for="nameField">Name</label>
							<input type="email" class="form-control ad-input" id="nameField" placeholder="Name">
							<label class="error">Enter Valid Name</label>
						</div>	
					</div>
					<div class="col-lg-6 col-md-12">
						<div class="form-group ad-form-group">
							<label class="nbfrm_label" for="emailField">Email</label>
							<input type="email" class="form-control ad-input" id="emailField" placeholder="Email">
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="form-group ad-form-group">
							<label class="nbfrm_label" for="phoneField">Phone</label>
							<input type="tel" class="form-control ad-input" id="phoneField" placeholder="Phone">
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="form-group ad-form-group">
							<label class="nbfrm_label" for="msgText">Message</label>
							<textarea class="form-control ad-input" id="msgText" rows="10" placeholder="Message"></textarea>
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="form-group ad-form-group text-center mt-4">
							<button type="submit" class="btn btn-primary">Send Message</button>
						</div>
					</div>
				</div>					
			</form>
		</div>
	</div>
</div>
<div class="cnt-map-sc">
	<div class="cnt-map-mn">
	   <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" width="100%" height="380" frameborder="0" style="border:0"></iframe>
	</div>
</div>	
<!-- Contact Us E -->
@endsection