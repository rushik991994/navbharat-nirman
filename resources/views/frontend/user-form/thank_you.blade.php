@extends('frontend.layouts.layout')
@section('content')
<div class="cnt-form-mn section-gap-padding">
    <div class="container">
       <div class="row">
          <div class="col-md-6 mx-auto">
             <div class="payment">
                <div class="payment_header">
                   <div class="check"><i class="fa fa-check" aria-hidden="true"></i></div>
                </div>
                <div class="content">
                   <h1>Payment Success !</h1>
                   <a href="{{route('home_page')}}">Go to Home</a>
                </div>
             </div>
          </div>
       </div>
    </div>
</div>

@endsection