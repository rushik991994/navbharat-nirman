@extends('frontend.layouts.layout')
@section('content')

	<!-- Inner Banner S -->
	<div class="inr-bnr-mn" style="background-image: url(assets/images/banner-2.jpg);">
		<div class="container">
			<div class="inr-bnr-sc">
				<h1 class="main-title">Apply Portal</h1>
			</div>
		</div>
	</div>
	<!-- Inner Banner E -->

	<!-- Contact Us S -->
		<div class="cnt-form-mn section-gap-padding">
			<div class="container">
				<div class="cnt-form-sc">
					<form class="get-in-form" id="customer_data_form">
						<div class="cnt-first" id="cnt-first">
							<div class="row">
								<div class="col-lg-12 col-md-12">
									<div class="form-group ad-form-group">
										<label class="nbfrm_label" for="phoneField">Phone<span class="text-danger"> *</span></label>
										<input type="tel" maxlength="10" class="form-control ad-input" id="phoneField" placeholder="Phone" name="phone_number">
										<label class="error" id="phoneField_error"></label>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 hide otp_div">
									<div class="form-group ad-form-group">
										<label class="nbfrm_label" for="phoneField">Verify OTP<span class="text-danger"> *</span></label>
										<input type="tel" maxlength="10" class="form-control ad-input" id="otpField" placeholder="Enter OTP" name="otp">
										<input type="hidden" name="unique_number" id="unique_number">
										<label class="error" id="otpField_error"></label>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 verify_otp hide">
									<div class="form-group ad-form-group text-center mt-4">
										<a href="javascript:void(0)" class="btn btn-secondary" id="verify-otp" onclick="verifyOTP()">Verify OTP</a>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 send_otp">
									<div class="form-group ad-form-group text-center mt-4">
										<a href="javascript:void(0)" class="btn btn-secondary send-otp" id="send-otp" onclick="sendOTP()">Send OTP</a>
									</div>
								</div>
							</div>
						</div>
						<div class="cnt-second" id="cnt-second">
							<div class="row mt-5">
								<div class="col-lg-12 col-md-12">
									<div class="ttl-sc">
										<h2 class="title main-title">Please fill detail</h2>
									</div>
								</div>
								<input type="hidden" name="new_or_not" id="new_or_not" value="">
								<div class="col-lg-6 col-md-12">
									<div class="form-group ad-form-group">
										<label class="nbfrm_label" for="fnameField">First Name<span class="text-danger"> *</span></label>
										<input type="text" class="form-control ad-input" id="fnameField" placeholder="First Name" name="first_name">
										<label class="error" id="first_name_error"></label>
									</div>	
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="form-group ad-form-group">
										<label class="nbfrm_label" for="lnameField">Last Name<span class="text-danger"> *</span></label>
										<input type="text" class="form-control ad-input" id="lnameField" placeholder="Last Name" name="last_name">
										<label class="error" id="last_name_error"></label>
									</div>	
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="form-group ad-form-group">
										<label class="nbfrm_label" for="enailField">Email Address<span class="text-danger"> *</span></label>
										<input type="email" class="form-control ad-input" id="enailField" placeholder="Email Address" name="email_id">
										<label class="error" id="email_id_error"></label>
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="form-group ad-form-group">
										<label class="nbfrm_label" for="agentCodeField">Agent Code</label>
										<input type="text" class="form-control ad-input" id="agentCodeField" placeholder="Agent Code" name="agent_code">
										<label class="error" id="agent_code_error"></label>
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="form-group ad-form-group">
										<label class="nbfrm_label" for="genderField">Gender<span class="text-danger"> *</span></label>
										<div class="ad-radio formRadio-grp d-flex flex-row flex-wrap align-items-center">
						                    <span class="formRadio-option  pb-2 mr-2">
						                        <input type="radio" name="gender" id="male" value="male" checked="checked">
						                        <label for="male">Male</label>
						                    </span>
						                    <span class="formRadio-option  pb-2 mr-2">
						                        <input type="radio" name="gender" id="female" value="female">
						                        <label for="female">Female</label>
						                    </span>
						                    <span class="formRadio-option  pb-2 mr-2">
						                        <input type="radio" name="gender" id="other" value="other">
						                        <label for="other">Other</label>
						                    </span>
						                </div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="form-group ad-form-group">
										<label class="nbfrm_label" for="dobField">Date of Birth<span class="text-danger"> *</span></label>
										<input type="date" class="form-control ad-input" id="dobField" placeholder="DD/MM/YYY" name="date_of_birth">
										<label class="error" id="date_of_birth_error"></label>
									</div>	
								</div>
								<div class="col-lg-6 col-md-12 capcha_required">
					                <div class="captcha form-group ad-form-group">
					                    <span>{!! captcha_img() !!}</span>
					                    <button type="button" class="small-btn btn-danger" id="reload">
					                        &#x21bb;
					                    </button>
					                </div>
					            </div>

					            <div class="col-lg-6 col-md-12 capcha_required">
					            	<div class="form-group ad-form-group">
						            	<label class="nbfrm_label" for="captcha">Enter Capcha<span class="text-danger"> *</span></label>
						                <input id="captcha" type="text" class="form-control ad-input" placeholder="Enter Captcha" name="captcha">
						                <label class="error" id="captcha_error"></label>
						            </div>
					            </div>
								<div class="col-lg-12 col-md-12">
									<div class="form-group ad-form-group text-center mt-4">
										<button type="submit" class="btn btn-primary get_in_form_btn">Submit & Pay</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<!-- Contact Us E -->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
	function validatePhoneNumber(input_str) {
	    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
	    return re.test(input_str);
	}

	function sendOTP(){
		var phone_number = $('#send-otp').parent().parent().parent().find('#phoneField').val();
		var send_otp_url = "<?= URL::route('otp.send') ?>";
		if(validatePhoneNumber(phone_number)){
			$.ajax({
				type: 'post',
				url: send_otp_url,
				dataType:'json',
				data: {
				    'phone_number' 	: phone_number,
	                '_token'		: '{{ csrf_token() }}'
				},
				success: function (results) {
					console.log(results);
					$('.otp_div').removeClass('hide');
					$('.verify_otp').removeClass('hide');
					$('.send_otp').addClass('hide');
					$('#unique_number').val(results.unique_number);
				}
			});
		}else{
			e.parent().parent().parent().find('#phoneField_error').text('Enter valid phone number');
		}
	}

	function verifyOTP(){
		var unique_number = $('#unique_number').val();
		var otp = $('#otpField').val();
		var send_otp_url = "<?= URL::route('otp.verify') ?>";
		$.ajax({
			type: 'post',
			url: send_otp_url,
			dataType:'json',
			data: {
			    'unique_number' : unique_number,
			    'otp'			: otp,
                '_token'		: '{{ csrf_token() }}'
			},
			success: function (results) {
				var phone_number = $('#phoneField').val();
				if(results.success == true){
					$('.otp_div').addClass('hide');
					$('.verify_otp').addClass('hide');
					$('.send_otp').removeClass('hide');
					$('#unique_number').val('');
					checkCustomerData(phone_number);
				}else{
					$('#otpField_error').text(results.message);
				}
			}
		});
	}

	function checkCustomerData(phone_number){
		var url = "<?= URL::route('user-form.get_user_data') ?>";
		$.ajax({
			type: 'post',
			url: url,
			dataType:'json',
			data: {
			    'phone_number' 	: phone_number,
                '_token'		: '{{ csrf_token() }}'
			},
			success: function (results) {
				$('.error').text('');
			  	displayData();
			  	$('#customer_data_form').trigger('reset');
			  	$('#phoneField').val(phone_number);
			  	if(results.success == false){
			  		$('#new_or_not').val('new');
			  		$('.capcha_required').show();
			  	}else{
			  		$('#new_or_not').val('old');
			  		$('.capcha_required').hide();
			  		var customer_data = results.customer_data;
					$('#fnameField').val(customer_data.first_name);
					$('#lnameField').val(customer_data.last_name);
					$('#enailField').val(customer_data.email_id);
					$('#agentCodeField').val(customer_data.agent_code);
					$('#dobField').val(customer_data.date_of_birth);
					var gender = customer_data.gender;
					$('#'+gender).prop('checked',true);
			  		if(customer_data.is_paid == 0){
			  			/* redirect that customer to payment page after auto fill form*/
						$('.get_in_form_btn').text('Submit & Pay with Razorpay');
			  		}else{
			  			/* redirect that customer to thank you page*/
						// $('.get_in_form_btn').text('Download Your Receipt');
						$('.get_in_form_btn').text('Submit');
			  		}
			  	}
			}
		});
	}

	$('body').on('click','.get_in_form_btn', function(e){
		e.preventDefault();
		$('.error').text('');
		var url = "<?= URL::route('user-form.save_data') ?>";
		var method_type = 'POST';
        var token = "{{csrf_token()}}";
        var first_name = $('#fnameField').val();
        var last_name = $('#lnameField').val();
		var email_id = $('#enailField').val();
		var date_of_birth = $('#dobField').val();
        if(first_name != "" && last_name != "" && email_id != "" && date_of_birth != ""){
	        $('#customer_data_form').ajaxSubmit({
	            url: url,
	            type: method_type,
	            data: {"_token" : token},
	            dataType: 'json',
	            beforeSubmit : function(){

	            },
	            success : function(resp){
	            	console.log('sucess',resp);
	            	// $('.message').text("Your data is saved");
	            	var phone_number = $('#phoneField').val();
	            	/*send customer to razorpay page for payment*/
	            	var customer_id = resp.customer_id;
	            	var is_paid = resp.is_paid;
	            	if(is_paid == 1){
				  		window.location.href = '/thank-you';
	            	}else{
			  			SendTocheckout(phone_number,first_name,last_name,email_id,customer_id);
	            	}
	            },
	            error : function(respObj){
	            	console.log('error',respObj);
	            	$.each(respObj.responseJSON.errors, function(k,v){
	                    $('#'+k+'_error').text(v);
	                });
	            }     
			});
        }else{
        	if(first_name == ""){
        		$('#first_name_error').text('First name is required');
        	}else if(last_name == ""){
        		$('#last_name_error').text('Last name is required');
        	}else if(email_id == ""){
        		$('#email_id_error').text('EmailID is required');
        	}else if(date_of_birth == ""){
        		$('#date_of_birth_error').text('Date of birth is required');
        	}
        }
	});

	function SendTocheckout(phone_number,first_name,last_name,email_id,customer_id){
		var options = {
		    "key": "{{ Config::get('custom.razor_key') }}",
		    "amount": "100", // Default currency is INR. Hence, 50000 refers to 50000 paise
		    "currency": "INR",
		    "name": "Navbharat Nirman",
		    "image": "{{ Config::get('custom.logo_url') }}",
		    "handler": function (response){
		    	console.log('razorpay response');
		    	console.log(response);
		    	sendToSaveData(response,customer_id);
		    },
		    "prefill": {
		        "name": first_name + " " + last_name,
		        "email": email_id,
		        "contact": phone_number
		    },
		    "theme": {
		        "color": "#3399cc"
		    }
		};
		var rzp1 = new Razorpay(options);
		rzp1.on('payment.failed', function (response){
		        // alert(response.error.code);
		        // alert(response.error.description);
		        // alert(response.error.source);
		        // alert(response.error.step);
		        // alert(response.error.reason);
		        // alert(response.error.metadata.order_id);
		        // alert(response.error.metadata.payment_id);
		});
	    rzp1.open();
	}

	function sendToSaveData(razorpay_response,customer_id){
		var sucess_url = "<?= URL::route('user-form.save_payment_data') ?>";
		$.ajax({
			type: 'post',
			url: sucess_url,
			dataType:'json',
			data: {
			    'razorpay_response' : razorpay_response,
			    'customer_id' 		: customer_id,
                '_token'			: '{{ csrf_token() }}'
			},
			success: function (results) {
				window.location.href = '/thank-you';
			}
		});
	}

	$('#reload').click(function () {
		var reload_url = "<?= URL::route('user-form.reload_captcha') ?>";
        $.ajax({
            type: 'GET',
            url: reload_url,
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });

	function displayData(){
		if ($(window).width() > 1023) {
			$("#cnt-second").fadeIn();
			$('html, body').animate({
				scrollTop: $("#cnt-second").offset().top - 180
			}, 500);
		}
		if ($(window).width() < 1024) {
			$("#cnt-second").fadeIn();
			$('html, body').animate({
				scrollTop: $("#cnt-second").offset().top - 50
			}, 500);
		}
	}
</script>

@endsection