<header>
    <div class="header-mn">
        <div class="topbar-mn">
            <div class="topbar-sc">
                <ul class="social-sc">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                </ul>
                <div class="cnt-dtl-sc">
                    <span>
                        <i class="fa fa-phone"></i>
                        <a href="javascript:void(0)">1800-320-2145</a>
                    </span>
                    <span>
                        <i class="fa fa-envelope"></i>
                        <a href="javascript:void(0)">reachus@navbharatnirman.com</a>
                    </span>
                    <span>
                        <i class="fa fa-user"></i>
                        Rera Agent No: <a href="javascript:void(0)">RAJ/A/2020/1683</a>
                    </span>
                </div>
            </div>
        </div>
        <div class="">
            <div class="header-sc">
                <a href="{{route('home_page')}}" class="logo-sc">
                    <img src="assets/images/logo.jpg" alt="Navbharat Nirman">
                </a>
                <div class="nav-sc">
                    <ul>
                        <li @if(Request::segment(1) == '') class="active" @endif>
                            <a href="{{route('home_page')}}">Home</a>
                        </li>
                        <li @if(Request::segment(1) == 'about-us') class="active" @endif>
                            <a href="{{route('about_us')}}">About us</a>
                        </li>
                        <li @if(Request::segment(1) == '/') class="active" @endif>
                            <a href="javascript:void(0)">Products</a>
                        </li>
                        <li @if(Request::segment(1) == 'contact-us') class="active" @endif>
                            <a href="{{route('contact_us')}}">Contact Us</a>
                        </li>
                        <li>
                            <a href="{{route('apply_portal')}}" class="btn btn-primary">Apply for Portal</a>
                        </li>
                    </ul>
                </div>
                <div class="icn-bars">
                    <div class="toggle-sc" id="nav-toggle">
                        <svg class="svg-icon" viewBox="0 0 20 20">
                            <path fill="#000000" d="M3.314,4.8h13.372c0.41,0,0.743-0.333,0.743-0.743c0-0.41-0.333-0.743-0.743-0.743H3.314
                                c-0.41,0-0.743,0.333-0.743,0.743C2.571,4.467,2.904,4.8,3.314,4.8z M16.686,15.2H3.314c-0.41,0-0.743,0.333-0.743,0.743
                                s0.333,0.743,0.743,0.743h13.372c0.41,0,0.743-0.333,0.743-0.743S17.096,15.2,16.686,15.2z M16.686,9.257H3.314
                                c-0.41,0-0.743,0.333-0.743,0.743s0.333,0.743,0.743,0.743h13.372c0.41,0,0.743-0.333,0.743-0.743S17.096,9.257,16.686,9.257z"></path>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>