<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0"/>
    <title>Navbharat Nirman</title>

    <!-- Favicon Icon S -->
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon"/>
    <!-- Favicon Icon E -->

    <!-- Canonical Link S -->
    <link rel="canonical" href=""/>
    <!-- Canonical Link E -->

    <!-- Style Sheet Link S -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Raleway:wght@400;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/libraries/bootstrap-4.0.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>
    <link rel="stylesheet" href="assets/libraries/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="assets/libraries/OwlCarousel2-2.2.1/css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="assets/libraries/OwlCarousel2-2.2.1/css/owl.theme.default.min.css"/>
    <link rel="stylesheet" href="assets/libraries/reset-css/reset.css"/>
    <link rel="stylesheet" href="assets/libraries/back-top/css/back-top.css"/>
    <!-- Style Sheet Link E -->

    <!-- Java Script Link S -->
    <script type="text/javascript" src="assets/libraries/jquery-3.2.1/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/libraries/jquery-3.2.1/js/jquery.form.min.js"></script>
    <!-- Java Script Link E -->
    <style>
        .hide{
            display: none;
        }
    </style>
</head>

<body>
    <!-- Scroll To Top S -->
    <div id="back_top" title="Scroll To Top" style="display: none;">
        <div class="icon">
            <svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #ffffff;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>
        </div>
    </div>
    <!-- Scroll To Top E -->

    <div id="wrapper">