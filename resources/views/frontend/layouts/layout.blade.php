@include('frontend.layouts.header')
@include('frontend.layouts.header_section')

@yield('content')

@include('frontend.layouts.footer_section')
@include('frontend.layouts.footer')
