        <!-- Java Script S -->
        <script src="assets/libraries/bootstrap-4.0.0/js/popper.min.js"></script>
        <script src="assets/libraries/bootstrap-4.0.0/js/bootstrap.min.js"></script>
        <script src="assets/libraries/OwlCarousel2-2.2.1/js/owl.carousel.min.js"></script>	
        <script src="assets/libraries/back-top/js/back-top.js"></script>
        <script src="assets/js/custom.js"></script>
        <!-- Java Script E -->

        <script>
            $(function(){
                /* Sticky header S */
                if ($(window).width() > 1023) {
                    var HeaderHeight = $(".header-mn").height();
                    $("#wrapper").css({"padding-top": HeaderHeight});
                    $(window).scroll(function() {
                        if ($(this).scrollTop() > HeaderHeight){
                            $('.header-mn').addClass("sticky");
                        }
                        else{
                            $('.header-mn').removeClass("sticky");
                        }
                    });
                }
                /* Sticky header E */

                /* Responsive Header S */
                if ($(window).width() < 1024) {
                    $("#nav-toggle").click(function(){
                        $(".header-sc .nav-sc").slideToggle();
                    });
                }
                /* Responsive Header E */
            });
        </script>
    </body>
</html>