@extends('backend.layouts.layout')
@section('content')
	
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Section</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add Section</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Edit Section</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              	
              	{!! Form::model($sections, ['route' => ['admin.section.update', $sections->id],'files'=>true,'method'=>'post'])  !!}
	                <div class="card-body">
	                  {{Form::token()}}
                    <div class="form-group">
                      <label for="title">Title</label>
                      <input type="text" class="form-control" name="title" id="title" value="{{$sections['title']}}" placeholder="Enter title">
                    </div>
	                  <div class="form-group">
	                    <label for="exampleInputFile">File input</label>
	                    <div class="input-group">
	                      <div class="custom-file">
	                        <input type="file" class="custom-file-input" name="image" id="exampleInputFile">
	                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
	                      </div>
	                      
	                    </div>
	                  </div>
                    
                    <img class="card-img-top" src="{{asset('backend/images/section_images/').'/'.$sections->image}}" alt="" height="200" style="width:500px;">
	                  <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status">
                            @if($sections['status'] == 1)
                            	<option value="1">Active</option>
                            	<option value="0">Inactive</option>
                            @else
                              <option value="0">Inactive</option>
                              <option value="1">Active</option>
                            @endif

                        </select>
                      </div>
	                  
	                </div>
	                <!-- /.card-body -->

	                <div class="card-footer">
	                  <button type="submit" class="btn btn-primary">Submit</button>
	                </div>
              	{!! Form::close() !!}
            </div>
            <!-- /.card -->

            

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script src="{{asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
@endsection