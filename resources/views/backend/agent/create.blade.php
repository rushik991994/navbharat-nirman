@extends('backend.layouts.layout')
@section('content')
	
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Agent</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add Agent</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Add Agent</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              	
              	{!! Form::open(['route' => 'admin.agent.store','files' => true,'method' => 'post']) !!}
	                <div class="card-body">
	                  {{Form::token()}}

                    <div class="form-group">
                      <label for="first_name">First Name</label>
                      <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('first_name') }}" placeholder="Enter First Name">
                      @if ($errors->has('first_name'))
                        <span class="help-block error">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="last_name">Last Name</label>
                      <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" id="last_name" placeholder="Enter Last Name">
                      @if ($errors->has('last_name'))
                        <span class="help-block error">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="phone">Mobile Number</label>
                      <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" id="mobile" placeholder="Enter Mobile Number" maxlength="10">
                        @if ($errors->has('mobile'))
                          <span class="help-block error">
                              <strong>{{ $errors->first('mobile') }}</strong>
                          </span>
                        @endif
                    </div>
                    <div class="form-group">
                      <label for="address">Address</label>
                      <textarea class="form-control" name="address" id="address" value="" rows="3" placeholder="Enter address">{{ old('address') }}</textarea>
                      @if ($errors->has('address'))
                        <span class="help-block error">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                      @endif
                    </div>
	                  
	                  
	                  
	                </div>
	                <!-- /.card-body -->

	                <div class="card-footer">
	                  <button type="submit" class="btn btn-primary">Submit</button>
	                </div>
              	{!! Form::close() !!}
            </div>
            <!-- /.card -->

            

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script src="{{asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
@endsection