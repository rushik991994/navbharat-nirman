@extends('backend.layouts.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banners          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Banners</li></ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Banners</h3>
                <div class="card-tools">
                  <a href="{{route('admin.banner.create')}}" class="btn btn-primary btn-sm">
                    <i class="fas fa-plus"></i> Add Banner
                  </a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<!-- DataTables  & Plugins -->
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
$(document).ready(function(){
	$("#example1").DataTable({
	  	"responsive": true, 
	  	"bProcessing": true,
	    "bServerSide": true,
	    "autoWidth": false,
	    "searching":true,
	    "aaSorting": [1,"desc"],
	    "sAjaxSource": "{{route('admin.banner.index')}}",
	    "aoColumns": [  
	    	{ mData:"id",bSortable : false,bVisible: false},
	    	{ mData:"image",bSortable : true,bVisible:true,sWidth:"55%",
	    		mRender:function(v,t,o){

	    			
	    			var url = '{{ URL::asset("/backend/images/banner_images") }}/'+o['image'];
	    			// console.log(url);
	    			return '<img class="card-img-top" src="'+url+'" alt="" height="200" style="width:500px;">';
	    		}
	    	},
			{ mData:"status",bSortable : true,bVisible:true,sWidth:"20%",
				mRender:function(v,t,o){
					var status= 'Active';
					var	color = 'bg-success';
					if(o['status'] == 0){
						status = 'Inactive';
						color = 'bg-danger';
					}
					status = '<span style="width:150px;height:32px;padding-top:8px;font-size:15px;" class="badge '+color+'">'+status+'</span>';
					return status;
				}
			},
	        { mData:"id",bSortable:false,sWidth:"20%", 
	        	mRender: function(v,t,o){

              var id=o['id'];
              var edit_path = "<?=URL::route('admin.banner.edit',':id')?>";

              var delete_path = "<?=URL::route('admin.banner.delete',':id')?>";

              console.log(edit_path);
              edit_path = edit_path.replace(':id',id);

              delete_path = delete_path.replace(':id',id);


      				


                var html = '<div class="form-button-action"><button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success" data-original-title="Edit Task" ><a href="'+edit_path+'"><i class="fa fa-edit" style="color:white;"></i></a></button><button  type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"><a href="'+delete_path+'"onclick="return conformDel('+id+')"<i class="fa fa-times " style="color:white;"></i></a></button></div>';


                // html += '<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger btn-sm" data-original-title="Remove"><a href="'+delete_path+'" class="btn btn-danger btn-sm" href="'+delete_path+'" onclick="return conformDel('+id+')" ><i class="fas fa-trash"></i>Delete</a></button>'


				        /*var html = '<div class="form-button-action"><button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"><a href="'+edit_path+'"><i class="fa fa-edit" title="edit"></i></a></button><button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"><a href="'+delete_path+'"onclick="return conformDel('+id+')"<i class="fa fa-trash" title="delete"></i></a></button></div>';*/
				      return html;
	        	}
	        },
	        
	    ],	  
	});
  

});
function conformDel(id) {
      // console.log(id);

  event.preventDefault();
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",

    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
   }).then(function (e) {

    // console.log(e.value);

  if (e.value === true) {
     // var id = $(this);
     // console.log(id);
      var url = "<?=URL::route('admin.banner.delete',':id')?>"; 
      url = url.replace(':id',id);

      $.ajax({
          type: 'get',
          url: url,
          dataType:'json',
          data: {
                id: id
                },
          success: function (results) {
            console.log(results);
              if (results.success === true) {
                
                  Swal.fire("Done!", results.message, "success");
                  var table= $('#example1').DataTable();

                  table.ajax.reload();
              } else {
                console.log('error');
                  Swal.fire("Error!", results.message, "error");
              }
          }
      });

    } else {
      e.dismiss;
      }
    });
    return false;
  }
</script>
@include('backend.layouts.alert');
@endsection
@section('style')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection