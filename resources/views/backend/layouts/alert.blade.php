<!-- @if(Session::has('success'))
	@if(Session::get('message_type'))
	
		<script type="text/javascript">
	        $(document).ready(function(){
		        toastr.success('{{ Session::get('success') }}');
	        });
	    </script>
    @endif
@endif
@if($errors->any())

    <script type="text/javascript">
        $(document).ready(function(){
            toastr.error("<b>There were some errors.</b>");
        });
    </script>
@endif
@if(Session::has('error'))
	@if(Session::get('message_type'))
	
		<script type="text/javascript">
	        $(document).ready(function(){
		       toastr.error("<b>These credentials do not match our records.</b>");
	        });
	    </script>
    @endif
@endif -->


<script>
  @if(Session::has('message'))
  toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true
  }
  		toastr.success("{{ session('message') }}");
  @endif

  @if(Session::has('error'))
  toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true
  }
  		toastr.error("{{ session('error') }}");
  @endif

  @if(Session::has('info'))
  toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true
  }
  		toastr.info("{{ session('info') }}");
  @endif

  @if(Session::has('warning'))
  toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true
  }
  		toastr.warning("{{ session('warning') }}");
  @endif
</script>