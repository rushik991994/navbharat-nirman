<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Navbharat</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{auth()->guard('admin')->user()->name}}</a>
        </div>
      </div>



      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
          <li class="nav-item ">
            <a href="#" class="nav-link @if(Request::segment(2) == '') active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashoard
                
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="{{route('admin.banner.index')}}" class="nav-link @if(Request::segment(2) == 'banner') active @endif">
              <i class="nav-icon far fa-image"></i>
              <p>
                Banners
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('admin.section.index')}}" class="nav-link @if(Request::segment(2) == 'section') active @endif">
              <i class="nav-icon far fa-image"></i>
              <p>
                Sections
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('admin.media.index')}}" class="nav-link @if(Request::segment(2) == 'media') active @endif">
              <i class="nav-icon far fa-image"></i>
              <p>
                Media
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('admin.agent.index')}}" class="nav-link @if(Request::segment(2) == 'agent') active @endif">
              <i class="nav-icon far fa-user-circle"></i>
              <p>
                Agent
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('admin.customers.index')}}" class="nav-link @if(Request::segment(2) == 'customers') active @endif">
              <i class="nav-icon far fa-user-circle"></i>
              <p>
                Customers
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('admin.logout')}}" class="nav-link">
              <i class="nav-icon fa fa-sign-out-alt"></i>
              <p>
                Logout
                
              </p>
            </a>
          </li>
          
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>