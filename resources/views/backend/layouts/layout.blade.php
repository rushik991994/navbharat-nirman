<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Dashboard</title>
  <style type="text/css">
  		.main-footer{
  			
	  	    position: fixed;
		    left: 0;
		    bottom: 0;
		    width: 100%;
  		}
  </style>
  @include('backend.layouts.style')
  @yield('style')
</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">

		<div class="preloader flex-column justify-content-center align-items-center">
	    	<img class="animation__shake" src="{{asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTELogo" height="60" width="60">
	  	</div>
	  	@include('backend.layouts.header')
	  	@include('backend.layouts.sidebar')

	  	@yield('content')

	  	@include('backend.layouts.footer')

	</div>
	@include('backend.layouts.script')
	@yield('script')

</body>
</html>