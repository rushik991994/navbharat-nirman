<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\BannerController;
use App\Http\Controllers\Backend\SectionController;
use App\Http\Controllers\Backend\MediaController;
use App\Http\Controllers\Backend\AgentController;
use App\Http\Controllers\Backend\LoginController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\CustomersController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {

//    return view('backend.layouts.layout');
// });

Route::get('/admin/login',[LoginController::class,'getLogin'])->name('admin.login');
Route::post('/admin/login',[LoginController::class,'postLogin'])->name('admin.login.post');

Route::namespace('Admin')->prefix('admin')->middleware('adminauth')->group( function(){

	Route::get('/dashboard',[DashboardController::class,'index'])->name('admin.dashboard');


	Route::get('/banner',[BannerController::class,'index'])->name('admin.banner.index');
	Route::get('/banner/create',[BannerController::class,'create'])->name('admin.banner.create');
	Route::post('/banner/store',[BannerController::class,'store'])->name('admin.banner.store');
	Route::get('/banner/edit/{id}',[BannerController::class,'edit'])->name('admin.banner.edit');
	Route::post('/banner/update/{id}',[BannerController::class,'update'])->name('admin.banner.update');
	Route::get('/banner/delete/{id}',[BannerController::class,'delete'])->name('admin.banner.delete');


	Route::get('/section',[SectionController::class,'index'])->name('admin.section.index');
	Route::get('/section/create',[SectionController::class,'create'])->name('admin.section.create');
	Route::post('/section/store',[SectionController::class,'store'])->name('admin.section.store');
	Route::get('/section/edit/{id}',[SectionController::class,'edit'])->name('admin.section.edit');
	Route::post('/section/update/{id}',[SectionController::class,'update'])->name('admin.section.update');
	Route::get('/section/delete/{id}',[SectionController::class,'delete'])->name('admin.section.delete');


	Route::get('/media',[MediaController::class,'index'])->name('admin.media.index');
	Route::get('/media/create',[MediaController::class,'create'])->name('admin.media.create');
	Route::post('/media/store',[MediaController::class,'store'])->name('admin.media.store');
	Route::get('/media/edit/{id}',[MediaController::class,'edit'])->name('admin.media.edit');
	Route::post('/media/update/{id}',[MediaController::class,'update'])->name('admin.media.update');
	Route::get('/media/delete/{id}',[MediaController::class,'delete'])->name('admin.media.delete');

	Route::get('/agent',[AgentController::class,'index'])->name('admin.agent.index');
	Route::get('agent/create',[AgentController::class,'create'])->name('admin.agent.create');
	Route::post('agent/store',[AgentController::class,'store'])->name('admin.agent.store');
	Route::get('agent/edit/{id}',[AgentController::class,'edit'])->name('admin.agent.edit');
	Route::post('agent/update/{id}',[AgentController::class,'update'])->name('admin.agent.update');
	Route::get('agent/delete/{id}',[AgentController::class,'delete'])->name('admin.agent.delete');

	Route::get('/customers',[CustomersController::class,'index'])->name('admin.customers.index');

	Route::get('logout',[LoginController::class,'logout'])->name('admin.logout');

});

// Route::get()
