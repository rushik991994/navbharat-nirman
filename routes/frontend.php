<?php

use App\Http\Controllers\Frontend\UserController;
use App\Http\Controllers\OTPController;


Route::get('/apply-portal',[UserController::class,'index'])->name('apply_portal');
Route::get('/', function () {
	return view('frontend.home');
})->name('home_page');
Route::get('/about-us', function () {
	return view('frontend.about_us');
})->name('about_us');
Route::get('/contact-us', function () {
	return view('frontend.contact_us');
})->name('contact_us');
Route::get('/thank-you', function () {
	return view('frontend.user-form.thank_you');
})->name('thank_you');
Route::post('/get-user-data',[UserController::class,'getUserData'])->name('user-form.get_user_data');
Route::post('/save-user-data',[UserController::class,'saveUserData'])->name('user-form.save_data');
Route::post('/save-user-payment-data',[UserController::class,'saveUserPaymentData'])->name('user-form.save_payment_data');
Route::get('/reload-captcha', [UserController::class, 'reloadCaptcha'])->name('user-form.reload_captcha');
Route::post('/send-otp', [OTPController::class, 'sendOTP'])->name('otp.send');
Route::post('/verify-otp', [OTPController::class, 'verifyOTP'])->name('otp.verify');