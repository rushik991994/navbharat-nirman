<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $request_all = $request->all();
        $rules = [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email_id'      => 'required',
            // 'agent_code'    => 'required',
            'gender'        => 'required',
            'date_of_birth' => 'required'
        ];
        if($request_all['new_or_not'] == 'new'){
            $rules['captcha'] = 'required|captcha';
        }
        return $rules;
    }

    public function messages(){
        return[
            'first_name.required' => 'First name is required',
            'last_name.required' => 'Last name is required',
            'email_id.required' => 'EmailID is required',
            'gender.required' => 'Gender name is required',
            'date_of_birth.required' => 'Date of birth is required',
            'captcha.required' => 'Please enter Captcha code',
            'captcha.captcha' => 'Please enter valid Captch code'
        ];
    }
}
