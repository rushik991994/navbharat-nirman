<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Models\Customer;
use App\Models\RazorpayLog;
use Illuminate\Support\Facades\Input;
use Razorpay\Api\Api;
use Session;
use Redirect;

class UserController extends Controller
{
    public function index(){
        return view('frontend.user-form.apply_for_portal');
    }

    public function getUserData(Request $request){
        $reqest_data = $request->all();
        $check_customer_data_query = Customer::where('phone_number',$reqest_data['phone_number']);

        $customer_count = $check_customer_data_query->count();

        if($customer_count > 0){
            $success = true;
            $customer_data = $check_customer_data_query->first()->toArray();
        }else{
            $success = false;
            $customer_data = [
                'first_name'    => '',
                'last_name'     => '',
                'email_id'      => '',
                'agent_code'    => '',
                'gender'        => '',
                'date_of_birth' => '',
                'is_paid'       => 0
            ];
        }

        return response()->json([
            'success' => $success,
            'customer_data' => $customer_data
        ],200);
    }

    public function saveUserData(UserRequest $request){
        $request_data = $request->all();
        $save_customer_data = Customer::firstOrNew(['phone_number' => $request_data['phone_number']]);
        $save_customer_data->fill($request_data);
        $save_customer_data->save();

        $customer_id = $save_customer_data->customer_id;
        $is_paid = $save_customer_data->is_paid;
        return response()->json(['success'=>true,'customer_id' => $customer_id,'is_paid' => $is_paid]);
    }

    public function saveUserPaymentData(Request $request){
        $response_data = $request->all();
        $razorpay_response = $response_data['razorpay_response'];
        $customer_id = $response_data['customer_id'];

        $update_customer_data = Customer::where('customer_id', $customer_id)
                                        ->update([
                                            'order_id' => $razorpay_response['razorpay_payment_id'],
                                            'payment_status' =>'paid',
                                            'is_paid' => 1
                                        ]);

        return response()->json(['success'=>true]);
    }

    public function sendToCheckOut(Request $request)
    {
        $request_data = $request->all();
        // echo "<pre>";
        // print_r($request_data);
        // exit();
        $api = new Api(RAZORPAY_API_KEY, RAZORPAY_API_SECRET);
        
        $razorpay_receipt = $this->generateRandomString();
        // Creates order
        $order_create = [
            'receipt' => $razorpay_receipt,
            'amount' => 100,
            'currency' => 'INR'
        ];
        $order = $api->order->create($order_create);
        
        $orderId = $order['id']; // Get the created Order ID
        $update_customer_data = Customer::where('phone_number', $request_data['phone_number'])
                                        ->update([
                                            'order_id' => $orderId,
                                            'razorpay_receipt' => $razorpay_receipt,
                                            'payment_status' =>'pending'
                                        ]);
        $save_razorpay_response = new RazorpayLog();
        $save_razorpay_response->order_request = json_encode($order_create);
        $save_razorpay_response->order_response = json_encode($order);
        $save_razorpay_response->save();

        // Customer::where('phone_number', $request_data['phone_number'])
        // return redirect()->route('payWithRazorpay',['email' => ]);
    }

    public function reloadCaptcha(){
        return response()->json(['captcha'=> captcha_img()]);
    }

    public function generateRandomString($length = 10){ 
        $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $string = ''; 
        for ($p = 0; $p < $length; $p++) { 
            $string .= $characters[mt_rand(0, strlen($characters) - 1)]; 
        } 
        return $string; 
    } 
}
