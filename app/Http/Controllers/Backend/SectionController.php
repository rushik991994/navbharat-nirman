<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sections;

class SectionController extends Controller
{
    public function index(Request $request){

    	if($request->ajax()){
            $where_str = '1 = ?';
            $where_params = [1];

	        if (!empty($request->input('sSearch')))
	        {
	            $search     = $request->input('sSearch');
	            $search = preg_replace('/[^a-zA-Z0-9_ -%][().][\/]/s', '', $search);
	            if($search != ''){
	            $where_str .= " and (title like \"%{$search}%\""
	            . ")";
	            }
	        }                                            

	        $columns = ['id','title','image','status'];


	        $sections = Sections::select($columns)
	        ->whereRaw($where_str, $where_params);  
	        
	        $sections_count = Sections::select($columns)
	        ->whereRaw($where_str, $where_params)
	        ->count();

	        if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
	        $sections = $sections->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
	        }   

	        if($request->input('iSortCol_0')){
	            for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
	            {
	                $column = $columns[$request->input('iSortCol_' . $i)];
	                if(false !== ($index = strpos($column, ' as '))){
	                	$column = substr($column, 0, $index);
	                }
	                $sections = $sections->orderBy($column,$request->input('sSortDir_'.$i));   
	            }
	        }  

	        $sections = $sections->get();
	        $response['iTotalDisplayRecords'] = $sections_count;
	        $response['iTotalRecords'] = $sections_count;
	        $response['sEcho'] = intval($request->input('sEcho'));
	        $response['aaData'] = $sections->toArray();
	        return $response;
        }
        return view('backend.section.index');

    }
    public function create(){

    	return view('backend.section.create');
    }

    public function store(Request $request){
    	


    	$request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title' => 'required',

        ]);
    	
        $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('backend/images/section_images'), $imageName);
  		
  		$sections  = new Sections;
  		$sections->title = $request->title;
  		$sections->image = $imageName;
  		$sections->status = $request['status'];
  		$sections->save();
        
        return redirect()->route('admin.section.index')->with('message','Section Added successfully');
            
    }
    public function edit($id){

    	$sections =  Sections::find($id);
        return view("backend.section.edit",compact('sections'));

    }
    public function update($id,Request $request){

    	$sections = Sections::find($id);
        
        if($request->hasfile('image'))
        {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('backend/images/section_images'), $imageName);   
            $sections->image=$imageName;
        
        }
        $sections->title=$request['title'];
        $sections->status=$request['status'];
        $sections->update();

        return redirect()->route('admin.section.index')->with('message','Section Updated successfully');
    }
    public function delete($id){

        $sections = Sections::find($id);
        
        $path = public_path('backend/images/section_images/'.$sections['image']);
        if(file_exists($path)){
            unlink($path);
        }
        $sections->delete();
        return response()->json(['success'=>true,'message'=>'Section is Removed']);

    }
}
