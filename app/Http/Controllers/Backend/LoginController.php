<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    // protected $redirectTo = '/admin/login';

    public function getLogin()
    {
        return view('backend.auth.login');
    }
    public function postLogin(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if (auth()->guard('admin')->attempt(['email' => $request['email'], 'password' =>  $request['password']]))
        {
        	
            $user = auth()->guard('admin')->user();
            
            return redirect()->route('admin.dashboard')->with('message','Login successfully!!');
            
        } else {
            return back()->with('error','Username or password are wrong');
        }

    }

    public function logout()
    {
        auth()->guard('admin')->logout();

        return redirect()->route('admin.login')->with('message','Logout successfully!!');
    }
}
