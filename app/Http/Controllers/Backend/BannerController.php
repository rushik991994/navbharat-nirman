<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banners;

class BannerController extends Controller
{
    public function index(Request $request){

    	if($request->ajax()){
            $where_str = '1 = ?';
            $where_params = [1];

	        /*if (!empty($request->input('sSearch')))
	        {
	            $search     = $request->input('sSearch');
	            $search = preg_replace('/[^a-zA-Z0-9_ -%][().][\/]/s', '', $search);
	            if($search != ''){
	            $where_str .= " and (title like \"%{$search}%\""
	            . ")";
	            }
	        }  */                                          

	        $columns = ['id','image','status'];


	        $banners = Banners::select($columns)
	        ->whereRaw($where_str, $where_params);  
	        
	        $banners_count = Banners::select($columns)
	        ->whereRaw($where_str, $where_params)
	        ->count();

	        if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
	        $banners = $banners->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
	        }   

	        if($request->input('iSortCol_0')){
	            for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
	            {
	                $column = $columns[$request->input('iSortCol_' . $i)];
	                if(false !== ($index = strpos($column, ' as '))){
	                $column = substr($column, 0, $index);
	                }
	                $banners = $banners->orderBy($column,$request->input('sSortDir_'.$i));   
	            }
	        }  

	        $banners = $banners->get();
	        $response['iTotalDisplayRecords'] = $banners_count;
	        $response['iTotalRecords'] = $banners_count;
	        $response['sEcho'] = intval($request->input('sEcho'));
	        $response['aaData'] = $banners->toArray();
	        return $response;
        }
        return view('backend.banners.index');

    }
    public function create(){

    	return view('backend.banners.create');
    }

    public function store(Request $request){
    	
    	$request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
    
        $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('backend/images/banner_images'), $imageName);
  		
  		$banners  = new Banners;
  		$banners->image = $imageName;
  		$banners->status = $request['status'];
  		$banners->save();
        
        return redirect()->route('admin.banner.index')->with('message','Banner Added successfully');
            
    }
    public function edit($id){

    	$banners =  Banners::find($id);
        return view("backend.banners.edit",compact('banners'));

    }
    public function update($id,Request $request){

    	$banners = Banners::find($id);
        
        if($request->hasfile('image'))
        {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('backend/images/banner_images'), $imageName);   
            $banners->image=$imageName;
        
        }
        $banners->status=$request['status'];
        $banners->update();

        return redirect()->route('admin.banner.index')->with('message','Banner Updated Successfully');
    }
    public function delete($id){

        $banner = Banners::find($id);
        
        $path = public_path('backend/images/banner_images/'.$banner['image']);
        if(file_exists($path)){
            unlink($path);
        }
        $banner->delete();
        return response()->json(['success'=>true,'message'=>'Banner is Removed']);

    }
}
