<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agent;

class AgentController extends Controller
{
    public function index(Request $request){

    	if($request->ajax()){
            $where_str = '1 = ?';
            $where_params = [1];

	        if (!empty($request->input('sSearch')))
	        {
	            $search     = $request->input('sSearch');
	            $search = preg_replace('/[^a-zA-Z0-9_ -%][().][\/]/s', '', $search);
	            if($search != ''){
	            $where_str .= " and (first_name like \"%{$search}%\""
            			." or last_name like  \"%{$search}%\""
            			." or mobile like  \"%{$search}%\""
            			." or address like  \"%{$search}%\""
	            . ")";
	            }
	        }                                            

	        $columns = ['id','first_name','last_name','mobile','address','agent_code'];


	        $agent = Agent::select($columns)
	        ->whereRaw($where_str, $where_params);  
	        
	        $agent_count = Agent::select($columns)
	        ->whereRaw($where_str, $where_params)
	        ->count();

	        if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
	        $agent = $agent->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
	        }   

	        if($request->input('iSortCol_0')){
	            for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
	            {
	                $column = $columns[$request->input('iSortCol_' . $i)];
	                if(false !== ($index = strpos($column, ' as '))){
	                	$column = substr($column, 0, $index);
	                }
	                $agent = $agent->orderBy($column,$request->input('sSortDir_'.$i));   
	            }
	        }  

	        $agent = $agent->get();
	        $response['iTotalDisplayRecords'] = $agent_count;
	        $response['iTotalRecords'] = $agent_count;
	        $response['sEcho'] = intval($request->input('sEcho'));
	        $response['aaData'] = $agent->toArray();
	        return $response;
        }
        return view('backend.agent.index');

    }
    public function create(){

    	return view('backend.agent.create');
    }

    public function store(Request $request){
    

    	$request->validate([
        'first_name' => 'required',
        'last_name' => 'required',
        'mobile' => 'required',
        'address' => 'required',

      ]);
    	
      $agent_code = substr(strtolower($request->first_name), 0, 3).rand(001,999);
  		
  		$agent  = new Agent;
  		$agent->first_name = $request->first_name;
  		$agent->last_name = $request->last_name;
      $agent->agent_code = $agent_code;
  		$agent->mobile = $request->mobile;
  		$agent->address = $request->address;
  		$agent->save();
        
      return redirect()->route('admin.agent.index')->with('message','Agent Added successfully');
            
    }
    public function edit($id){

    	$agent =  Agent::find($id);
        return view("backend.agent.edit",compact('agent'));

    }
    public function update($id,Request $request){

    	$agent = Agent::find($id);
        
        
      $agent->first_name = $request->first_name;
  		$agent->last_name = $request->last_name;
  		$agent->mobile = $request->mobile;
  		$agent->address = $request->address;

        $agent->update();

        return redirect()->route('admin.agent.index')->with('message','Agent Updated successfully');
    }
    public function delete($id){

        $agent = Agent::find($id);
        $agent->delete();

        return response()->json(['success'=>true,'message'=>'Agent is Removed']);

    }
}
