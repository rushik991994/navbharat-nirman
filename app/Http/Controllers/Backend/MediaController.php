<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Media;

class MediaController extends Controller
{
    public function index(Request $request){

    	if($request->ajax()){
            $where_str = '1 = ?';
            $where_params = [1];

	        
	        $columns = ['id','image','status'];


	        $media = Media::select($columns)
	        ->whereRaw($where_str, $where_params);  
	        
	        $media_count = Media::select($columns)
	        ->whereRaw($where_str, $where_params)
	        ->count();

	        if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
	        $media = $media->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
	        }   

	        if($request->input('iSortCol_0')){
	            for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
	            {
	                $column = $columns[$request->input('iSortCol_' . $i)];
	                if(false !== ($index = strpos($column, ' as '))){
	                $column = substr($column, 0, $index);
	                }
	                $media = $media->orderBy($column,$request->input('sSortDir_'.$i));   
	            }
	        }  

	        $media = $media->get();
	        $response['iTotalDisplayRecords'] = $media_count;
	        $response['iTotalRecords'] = $media_count;
	        $response['sEcho'] = intval($request->input('sEcho'));
	        $response['aaData'] = $media->toArray();
	        return $response;
        }
        return view('backend.media.index');

    }
    public function create(){

    	return view('backend.media.create');
    }

    public function store(Request $request){
    	
    	$request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
    
        $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('backend/images/media_images'), $imageName);
  		
  		$media  = new Media;
  		$media->image = $imageName;
  		$media->status = $request['status'];
  		$media->save();
        
        return redirect()->route('admin.media.index')->with('message','Media Added successfully');
            
    }
    public function edit($id){

    	$media =  Media::find($id);
        return view("backend.media.edit",compact('media'));

    }
    public function update($id,Request $request){

    	$media = Media::find($id);
        
        if($request->hasfile('image'))
        {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('backend/images/media_images'), $imageName);   
            $media->image=$imageName;
        
        }
        $media->status=$request['status'];
        $media->update();

        return redirect()->route('admin.media.index')->with('message','Media Updated successfully');
    }
    public function delete($id){

        $media = Media::find($id);
        
        $path = public_path('backend/images/media_images/'.$media['image']);
        if(file_exists($path)){
            unlink($path);
        }
        $media->delete();
        return response()->json(['success'=>true,'message'=>'Media is Removed']);

    }
}
