<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;

class CustomersController extends Controller
{
    public function index(Request $request){

    	if($request->ajax()){
            $where_str = '1 = ?';
            $where_params = [1];

	        if (!empty($request->input('sSearch')))
	        {
	            $search     = $request->input('sSearch');
	            $search = preg_replace('/[^a-zA-Z0-9_ -%][().][\/]/s', '', $search);
	            if($search != ''){
	            $where_str .= " and (first_name like \"%{$search}%\""
            			." or last_name like  \"%{$search}%\""
            			." or email_id like  \"%{$search}%\""
            			." or phone_number like  \"%{$search}%\""
            			." or paid_date like  \"%{$search}%\""
	            . ")";
	            }
	        }                                            

	        $columns = ['customer_id','first_name','last_name','email_id','phone_number','paid_date'];


	        $customer = Customer::select($columns)
	        ->whereRaw($where_str, $where_params);  
	        
	        
	        $customer_count = Customer::select($columns)
	        ->whereRaw($where_str, $where_params)
	        ->count();

	        if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
	        $customer = $customer->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
	        }   

	        if($request->input('iSortCol_0')){
	            for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
	            {
	                $column = $columns[$request->input('iSortCol_' . $i)];
	                if(false !== ($index = strpos($column, ' as '))){
	                	$column = substr($column, 0, $index);
	                }
	                $customer = $customer->orderBy($column,$request->input('sSortDir_'.$i));   
	            }
	        }  

	        $customer = $customer->get();
	        $response['iTotalDisplayRecords'] = $customer_count;
	        $response['iTotalRecords'] = $customer_count;
	        $response['sEcho'] = intval($request->input('sEcho'));
	        $response['aaData'] = $customer->toArray();
	        return $response;
        }
        return view('backend.customer.index');

    }
}
