<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SMSLog;
use App\Jobs\SendSMSJob;

class OTPController extends Controller
{
    public function sendOTP(Request $request){
        $request_data = $request->all();
        $otp = mt_rand(100000, 999999);
        $timestamp = date('Ymdhis');
        // $message =  SMS_OTP_TEXT . $otp . '.';
        $message = "Dear Gautam\n\nYour one time password (OTP) is ". $otp .".Please do not share with this anyone.\n\nThank you,\nNavbharat Nirman";
        $sms_url = SMS_API . "4&Mobile=+91". $request_data['phone_number'] ."&MsgText=". urlencode($message) ."&EntityID=1301162833551392887&TemplateID=1307162884545521149";
        $unique_number = $request_data['phone_number'] . date('Ymdhis');

        $sms_data = [
            'sms_url' => $sms_url
        ];
        // $response_sms = "";
        $response_sms = dispatch(new SendSMSJob($sms_data));

        $save_otp_data = [
            'type' => 'OTP',
            'otp' => $otp,
            'message' => $message,
            'mobile_number' => $request_data['phone_number'],
            'unique_number' => $unique_number
        ];

        SMSLog::create($save_otp_data);

        return response()->json([
            'success' => true,
            'unique_number' => $unique_number
        ],200);
    }

    public function verifyOTP(Request $request)
    {
        $request_data = $request->all();
        $unique_number = $request_data['unique_number'];
        $entered_otp = $request_data['otp'];
        $check_otp_query = SMSLog::where('unique_number',$unique_number)
                                ->where('otp',$entered_otp)
                                ->where('is_verify',0);
        $check_otp_count = $check_otp_query->count();
        if($check_otp_count > 0){
            $success = true;
            $message = 'OTP verified';
            $check_otp_query->update(['is_verify' => 1]);
        }else{
            $success = false;
            $message = 'OTP not verified';
        }
        return response()->json(['success'=>$success,'message'=>$message],200);
    }
}
