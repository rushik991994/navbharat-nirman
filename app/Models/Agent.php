<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    // use HasFactory;
    protected $table_name = 'agents';

	protected $fillable = ['first_name','last_name','mobile','address','agent_code'];
}
