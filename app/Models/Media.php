<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    // use HasFactory;
    protected $table_name = 'media';

	protected $fillable = ['image','status'];
}
