<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SMSLog extends Model
{
    protected $table = 'sms_logs';
    protected $fillable = ['type','message','mobile_number','otp','is_verify','unique_number'];
}
