<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RazorpayLog extends Model
{
    protected $table = 'razorpay_reponses';
    protected $fillable = ['order_id','order_request','order_response'];
}
