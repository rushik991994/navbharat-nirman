<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

	// use HasFactory;
	
    protected $table_name = 'customers';


    protected $primaryKey = "customer_id";

	protected $fillable = ['phone_number','first_name','last_name','email_id','gender','date_of_birth','is_paid','paid_date','receipt','agent_code','order_id','razorpay_receipt','payment_status'];
}
