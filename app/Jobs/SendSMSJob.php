<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSMSJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $sms_data;
    public function __construct($sms_data)
    {
        $this->sms_data = $sms_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sms_data = $this->sms_data;
        $sms_url = $sms_data['sms_url'];
        $ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,$sms_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output =curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}
